module.exports = {
    dialect:'postgres',
    host:'localhost',
    username:'postgres',
    password:'',
    database:'ioasys',
    define:{
        timestamps:true,
        underscored:true,
        underscoredAll:true,
    },


};
